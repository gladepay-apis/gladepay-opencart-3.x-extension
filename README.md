# OpenCart Extension for Gladepay 

## Description
OpenCart Gladepay payment gateway integration. Visit the [Gladepay Website](http://gladepay.com) to know how gladepay works.
Install to receive payments for your goods in naira from any Mastercard, Visa or Verve card in the world.

## Requirements
- Curl 7.34.0 or more recent
- PHP 5.5.19 or more recent
- OpenSSL v1.0.1 or more recent

## Notes
- Gladepay currently only accepts the following currencies: `NGN`.
- You need to have created an account on [gladepay.com](https://dashboard.gladepay.com/register).

## Features:
- Gladepay payment gateway integration
- Activates payment module only when cart currency is `NGN`.
- Activate payment module only when order total reaches the amount you specified
- Captures call back notification to automatically update order status
- Simply turn on Live Mode to accept live payments.

## Installation
0. Visit [Gladepay OpenCart Extension](http://www.opencart.com/index.php?route=extension/extension/info&extension_id=25767&filter_search=gladepay) to download the latest release.
1. Unzip the files. Select the right version for your OpenCart.
2. Upload the files to your OpenCart installation folder with a FTP client 
                     `OR`
3. Upload the zipped file to the root of your OpenCart installation then unzip using cPanel File Manager. The folders should merge. 
4. In admin panel, proceed to ‘Extensions > Payment’ and install ‘Gladepay’.
5. Configure the module accordingly. 
 - To Demo or Test use 
  ``
   Merchant Key: GP0000001 
	``
	``
   Merchant Secret: 123456789
	``
	``
   See Demo Test Credentials Update At: https://developer.gladepay.com
  ``
 - To get your live keys, visit [the Gladepay Dashboard](https://dashboard.gladepay.com).
6. Enable Gladepay payment gateway on your OpenCart admin.
7. Set NGN as your default store currency.
8. Proceed to [Gladepay OpenCart Extension](http://www.opencart.com/index.php?route=extension/extension/info&extension_id=25767&filter_search=gladepay) to rate our work.

## Other Configuration
To add currencies, you can do so at System > Localisation > Currencies. 


## Contributors
- Light Chinaka
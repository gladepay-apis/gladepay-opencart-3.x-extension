<?php
// Heading
$_['heading_title'] = 'Gladepay';

// Text
$_['text_payment'] = 'Payment';
$_['text_success'] = 'Success: You have modified/updated Gladepay details!';
$_['text_edit'] = 'Edit Gladepay';
$_['text_gladepay'] = '<a target="_BLANK" href="http://www.gladepay.com"><img src="view/image/payment/gladepay.png" alt="Gladepay" title="Gladepay" style="border: 1px solid #cccccc;" /></a>';
$_['text_pay'] = 'Pay';
$_['text_disable_payment'] = 'Disable Payment Method';

// Entry
$_['entry_live_merchant_secret'] = 'Merchant Secret:';
$_['entry_live_merchant_key'] = 'Merchant Key:';
$_['entry_demo_merchant_secret'] = 'Demo Merchant Secret:';
$_['entry_demo_merchant_key'] = 'Demo Test Merchant Key:';

$_['entry_live'] = 'Live Mode';
$_['entry_debug'] = 'Debug Mode';
$_['entry_total'] = 'Total';
$_['entry_approved_status'] = 'Approved Status';
$_['entry_declined_status'] = 'Declined Status:';
$_['entry_error_status'] = 'Error Status:';
$_['entry_geo_zone'] = 'Geo Zone';
$_['entry_status'] = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Tab
$_['tab_general'] = 'General';
$_['tab_order_status'] = 'Order Status';

// Help
$_['help_live'] = 'Use the live mode to process transactions? Request Go Live on the Gladepay Dashboard to get live keys.';
$_['help_debug'] = 'Logs additional information to the system log';
$_['help_total'] = 'The checkout total the order must reach before this payment method becomes active';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify the Gladepay module!';
$_['error_test_keys'] = 'Both Demo key and ID are required for test mode!';
$_['error_live_keys'] = 'Both Live key and ID are required for live mode!';

<?php
class ControllerExtensionPaymentGladepay extends Controller
{
    public function index() 
    {
        $this->load->model('checkout/order');

        $this->load->language('extension/payment/gladepay');

        $data['button_confirm'] = $this->language->get('button_confirm');

        $data['text_testmode'] = $this->language->get('text_testmode');
        $data['livemode'] = $this->config->get('payment_gladepay_live');

        if ($this->config->get('payment_gladepay_live')) {
            $data['key'] = $this->config->get('payment_gladepay_live_merchant_public');
        } else {
            $data['key'] = $this->config->get('payment_gladepay_demo_merchant_public');
        }

        $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        $data['currency'] = $order_info['currency_code'];
        $data['ref']      = uniqid('' . $this->session->data['order_id'] . '-');
        $data['amount']   = intval($order_info['total'] * 100);
        $data['email']    = $order_info['email'];
        $data['callback'] = $this->url->link('extension/payment/gladepay/callback', 'trxref=' . rawurlencode($data['ref']), 'SSL');

        return $this->load->view('extension/payment/gladepay', $data);
    }

    private function query_api_transaction_verify($reference) 
    {
        if ($this->config->get('payment_gladepay_live')) {
            $gM_ID_Key = $this->config->get('payment_gladepay_live_merchant_public');
            $gM_Secret = $this->config->get('payment_gladepay_live_merchant_public');
            $base_url = "https://api.gladepay.com/payment";
        } else {
            $gM_ID_Key = $this->config->get('payment_gladepay_demo_merchant_public');
            $gM_Secret = $this->config->get('payment_gladepay_demo_merchant_secret');
            $base_url = "https://demo.api.gladepay.com/payment";
        }

        $context = stream_context_create(
            array(
                'http'=>array(
                'method'=>"PUT",
                'header'=>"Authorization: Bearer " .  $gM_ID_Key,
                )
            )
        );

        $data_array = array(
            'action' => 'verify' , 
            'txnRef' => $reference 
        );

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "$base_url",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "PUT",
          CURLOPT_POSTFIELDS => $data_array,
          CURLOPT_HTTPHEADER => array(
            "key: $gM_Secret",
            "mid: $gM_ID_Key"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        // if ($err) {
        //   echo "cURL Error #:" . $err;
        // } else {
        //   echo $response;
        // }

        return json_decode($response, true);
    }

    private function redir_and_die($url, $onlymeta = false)
    {
        if (!headers_sent() && !$onlymeta) {
            header('Location: ' . $url);
        }
        echo "<meta http-equiv=\"refresh\" content=\"0;url=" . addslashes($url) . "\" />";
        die();
    }

    public function callback()
    {
        if (isset($this->request->get['txnRef'])) {
            $trxref = $this->request->get['txnRef'];

            // order id is what comes before the first dash in trxref
            $order_id = substr($trxref, 0, strpos($trxref, '-'));
            // if no dash were in transation reference, we will have an empty order_id
            if (!$order_id) {
                $order_id = 0;
            }

            $this->load->model('checkout/order');

            $order_info = $this->model_checkout_order->getOrder($order_id);

            if ($order_info) {
                if ($this->config->get('payment_gladepay_debug')) {
                    $this->log->write('GLADEPAY :: CALLBACK DATA: ' . print_r($this->request->get, true));
                }

                // Callback gladepay to get real transaction status
                $ps_api_response = $this->query_api_transaction_verify($trxref);

                $order_status_id = $this->config->get('config_order_status_id');

                if (array_key_exists('data', $ps_api_response) && array_key_exists('status', $ps_api_response['data']) && ($ps_api_response['data']['status'] === 'success')) {
                    $order_status_id = $this->config->get('payment_gladepay_approved_status_id');
                    $redir_url = $this->url->link('checkout/success');
                } else if (array_key_exists('data', $ps_api_response) && array_key_exists('status', $ps_api_response['data']) && ($ps_api_response['data']['status'] === 'failure')) {
                    $order_status_id = $this->config->get('payment_gladepay_declined_status_id');
                    $redir_url = $this->url->link('checkout/checkout', '', 'SSL');
                } else {
                    $order_status_id = $this->config->get('gladepay_error_status_id');
                    $redir_url = $this->url->link('checkout/checkout', '', 'SSL');
                }

                $this->model_checkout_order->addOrderHistory($order_id, $order_status_id);
                $this->redir_and_die($redir_url);
            }

        }


    }
}
